package com.waterseven.wso.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class WsoConfigurationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsoConfigurationServerApplication.class, args);
	}

}
